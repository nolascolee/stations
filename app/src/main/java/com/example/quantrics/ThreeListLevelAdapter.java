package com.example.quantrics;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.LinkedHashMap;
import java.util.List;

public class ThreeListLevelAdapter extends BaseExpandableListAdapter {

    List<Stop> parentHeaders;
    List<Routes> secondLevel;
    List<LinkedHashMap<String, String[]>> data;
    private Context context;

    public ThreeListLevelAdapter(Context context, List<Stop> parentHeader, List<Routes> secondLevel) {
        this.context = context;

        this.parentHeaders = parentHeader;

        this.secondLevel = secondLevel;

    }

    @Override
    public int getGroupCount() {
        if (parentHeaders == null) {
            return 0;
        } else
            return parentHeaders.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return 1;

    }

    @Override
    public Object getGroup(int groupPosition) {

        return groupPosition;
    }

    @Override
    public Object getChild(int group, int child) {


        return child;


    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.list_view_item, null);
        TextView text = (TextView) convertView.findViewById(R.id.textView3);
        text.setText(this.parentHeaders.get(groupPosition).getName());

        if (isExpanded) {

        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final SecondLevelExpandableListView secondLevelELV = new SecondLevelExpandableListView(context);

//        String[] headers = secondLevel.get(groupPosition);
//
//
//        List<String[]> childData = new ArrayList<>();
//        HashMap<String, String[]> secondLevelData=data.get(groupPosition);
//
//        for(String key : secondLevelData.keySet())
//        {
//
//
//            childData.add(secondLevelData.get(key));
//
//        }
        Stop stop = parentHeaders.get(groupPosition);
        List<Routes> routes = stop.getRoutes();


        secondLevelELV.setAdapter(new SecondLevelAdapter(context, routes));

        secondLevelELV.setGroupIndicator(null);


        secondLevelELV.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    secondLevelELV.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });


        return secondLevelELV;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}