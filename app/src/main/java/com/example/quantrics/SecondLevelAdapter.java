package com.example.quantrics;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;


public class SecondLevelAdapter extends BaseExpandableListAdapter {

    //    List<String[]> data;
//
//    String[] headers;
    List<Routes> data;
    List<StopTimes> stopTimes;
    private Context context;


    public SecondLevelAdapter(Context context, List<Routes> data) {
        this.context = context;
        this.data = data;

    }

    @Override
    public Object getGroup(int groupPosition) {

        return data.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        if (data.size() == 0) {
            return 0;
        } else
            return data.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.list_view_item_route, null);
        TextView routename = (TextView) convertView.findViewById(R.id.tv_route_name);
//        String groupText = getGroup(groupPosition).toString();
        TextView routeId = (TextView) convertView.findViewById(R.id.tv_route_group_id);
        routename.setText("Route: " + this.data.get(groupPosition).getName());
        routeId.setText("ID: " + this.data.get(groupPosition).getGroupID());

        return convertView;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        List<StopTimes> stopTimes = data.get(groupPosition).getStop_times();


        return stopTimes.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.list_view_item_stoptime, null);

        TextView shape = (TextView) convertView.findViewById(R.id.tv_st_shape);
        TextView dept_time = (TextView) convertView.findViewById(R.id.tv_st_dept_time);
        TextView service_id = (TextView) convertView.findViewById(R.id.tv_st_service_id);
        TextView timestamp = (TextView) convertView.findViewById(R.id.tv_st_dts);


        StopTimes stopTimes = (StopTimes) getChild(groupPosition, childPosition);


        shape.setText("Shape: " + stopTimes.getShape());
        dept_time.setText("Departure Time: " + stopTimes.getDeparture_time());
        service_id.setText("Service ID: " + String.valueOf(stopTimes.getServiceID()));
        timestamp.setText("Time Stamp: " + String.valueOf(stopTimes.getDept_timeStamp()));

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        List<StopTimes> stopTimes = data.get(groupPosition).getStop_times();
        if (stopTimes.size() == 0) {
            return 0;
        } else
            return stopTimes.size();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
