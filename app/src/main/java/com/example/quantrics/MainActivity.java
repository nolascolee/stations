package com.example.quantrics;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {
    TextView tv1, station_name, agency;
    String url = "https://myttc.ca/";
    List<Stop> stops;
    List<Routes> routes;
    List<StopTimes> stoptimes;
    ArrayList<String> array_stop;
    ExpandableListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        init();

    }


    private void init() {
        tv1 = findViewById(R.id.textView2);
        station_name = findViewById(R.id.textView2);
        agency = findViewById(R.id.textView4);
        lv = findViewById(R.id.e);

        Retrofit retro = new Retrofit.Builder().baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create()).build();

        GetData getStation = retro.create(GetData.class);

        Call<Station> call = getStation.getData();


        call.enqueue(new Callback<Station>() {
            @Override
            public void onResponse(Call<Station> call, Response<Station> response) {
                if (!response.isSuccessful()) {
                    tv1.setText("Code: " + response.code());
                } else {
                    Station station = response.body();
                    stops = station.getStops();
                    station_name.setText(station.getName());
                    agency.setText(stops.get(0).getAgency());
                    Log.v("STOPS: ", stops.get(3).getName());
                    for (Stop stop : stops) {
                        routes = stop.getRoutes();
                        for (Routes route : routes) {
                            stoptimes = route.getStop_times();
                        }
                    }
                    ThreeListLevelAdapter threeListLevelAdapter = new ThreeListLevelAdapter(getApplicationContext(), stops, routes);
                    threeListLevelAdapter.notifyDataSetChanged();
                    lv.setAdapter(threeListLevelAdapter);

                }
            }

            @Override
            public void onFailure(Call<Station> call, Throwable t) {
                tv1.setText(t.getMessage());
            }
        });

        ThreeListLevelAdapter threeListLevelAdapter = new ThreeListLevelAdapter(this, stops, routes);
        lv.setAdapter(threeListLevelAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
