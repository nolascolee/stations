package com.example.quantrics;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetData {

    @GET("finch_station.json")
    Call<Station> getData();

}
