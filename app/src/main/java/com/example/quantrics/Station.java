package com.example.quantrics;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Station {
    @SerializedName("time")
    @Expose
    public int time;
    @SerializedName("stops")
    @Expose
    public List<Stop> stops = null;
    @SerializedName("uri")
    @Expose
    public String uri;
    @SerializedName("name")
    @Expose
    public String name;

    public int getTime() {
        return time;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public String getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }
}

class Stop {
    @SerializedName("routes")
    @Expose
    public List<Routes> routes = null;

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("uri")
    @Expose
    public String uri;
    @SerializedName("agency")
    @Expose
    public String agency;

    public List<Routes> getRoutes() {
        return routes;
    }

    public String getName() {
        return name;
    }

    public String getUri() {
        return uri;
    }

    public String getAgency() {
        return agency;
    }


}

class Routes {
    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("uri")
    @Expose
    public String uri;

    @SerializedName("route_group_id")
    @Expose
    public String groupID;
    @SerializedName("stop_times")
    @Expose
    public List<StopTimes> stop_times = null;

    public String getName() {
        return name;
    }

    public String getUri() {
        return uri;
    }

    public String getGroupID() {
        return groupID;
    }

    public List<StopTimes> getStop_times() {
        return stop_times;
    }

}

class StopTimes {
    @SerializedName("departure_time")
    @Expose
    public String departure_time;

    @SerializedName("shape")
    @Expose
    public String shape;
    @SerializedName("departure_timestamp")
    @Expose
    public int dept_timeStamp;
    @SerializedName("service_id")
    @Expose
    public int serviceID;

    public String getDeparture_time() {
        return departure_time;
    }

    public String getShape() {
        return shape;
    }

    public int getDept_timeStamp() {
        return dept_timeStamp;
    }

    public int getServiceID() {
        return serviceID;
    }
}
